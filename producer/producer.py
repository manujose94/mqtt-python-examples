
# Import package
import json
import sys
import paho.mqtt.client as mqtt
import time,datetime
from random import uniform
# Define Variables
MQTT_HOST = "0.0.0.0"
MQTT_PORT = 1883
MQTT_KEEPALIVE_INTERVAL = 45
MQTT_TOPIC = "tracker"
MQTT_MSG=json.dumps({"timestamp": "6.4","temperature":  "3.2","humidity": "4.5"}); # json to json string, Python dictionary. json.loads ()
MQTT_CLIENT_NAME="MYPRODUCER"
def generate_payload()-> str:
    timestamp=int( time.time() ) # Second Precision
    payload = json.loads(MQTT_MSG)# Python dictionary
    payload['timestamp']=timestamp
    payload['temperature']=round(uniform(10.33, 30.35), 2)
    payload['humidity']=round(uniform(20.33, 25.35), 2)
    # Dictionary to JSON Object using dumps() method
    # Return JSON Object formatted str
    return json.dumps(payload)

    # now_ms = int( time.time_ns() / 1000 ) # MSeconds Precision
# Define on_publish event function
def on_publish(client, userdata, mid):
    
    print(f"[PRODUCER {client}] Message Published...")

# Initiate MQTT Client
MQTT = mqtt.Client(MQTT_CLIENT_NAME)

# Register publish callback function
MQTT.on_publish = on_publish

print(generate_payload())
# Connect with MQTT Broker
try:
        MQTT.connect(MQTT_HOST, MQTT_PORT, MQTT_KEEPALIVE_INTERVAL)	
except ConnectionRefusedError as e:
        print("[ERROR] Connection Error to Broker MQTT")
        exit(1)



for i in range(360):
    try:
        MQTT.publish(MQTT_TOPIC,generate_payload())
        time.sleep(3)
    except KeyboardInterrupt:
        MQTT.disconnect()
        sys.exit(0)	

# Disconnect from MQTT_Broker
MQTT.disconnect()

