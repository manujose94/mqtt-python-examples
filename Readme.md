# MQTT and PYTHON

Small project to show some examples of messaging using queues, this is implemented in many IoT solutions. In this particular case, MQTT is used. These examples consist of:

- 1- Dockerfile for the deployment of a container that acts as a MQTT broker.
- 2- Some simple producers and consumers
- 3- A visualization implementation with Flask, MQTT and SockerIO.


## SetUp

In case you want to install all packages yourself:

```shell
# pwd -> /consumer or /producer
python3 -m venv venv
source venv/bin/activate
venv/bin/pip install paho
venv/bin/pip install paho-mqtt
venv/bin/pip install <package>

#Generated requirements
pip freeze > requirements.txt
```

To prepare the environment automatically:
``
source venv/bin/activate
pip install -r requirements.txt
``

### Using the RabbitMQ Docker Image

Build and Launch:
```
docker build -t mamarbao/rabbitmqtt .
docker run --rm -it --hostname my-rabbit -p 15672:15672 -p 15675:15675  -p 5672:5672 -p 1883:1883 mamarbao/rabbitmqtt
```
Launch individual container:

```
docker run --rm -it --hostname my-rabbit -p 15672:15672 -p 5672:5672 rabbitmq:3-management
```

The command above, the port 5672 is used for the RabbitMQ client connections, and the port 15672 is for the RabbitMQ management website. This command may take a minute to execute. After that, we can open a browser window and use the URL http://localhost:15672 (dy deafult: guest user/pass) to visit the RabbitMQ management website. 



### Docker Compose

In this example of docker-compose.yml there are volumes in order to maintain the MQTT broker configuration when relaunching via docker-copose. The volumes are built into the folder structure where the docker.compose.yaml file is located, here the folder structure:

```shell
rabbitmq/
├── data
├── etc
│   ├── definitions.json
│   └── rabbitmq.conf
└── logs

```

> Note: The **definitions.json** file is generated via website of broker (http://localhost:15672)
>

```shell
docker-compose up -d # run all services
#or
docker-compose up --build # to force a rebuild, add --no-cache if there is any change in Dockerfile
```

If it be want only the service app to be rebuilt and restarted when there is a code change. This is a two-step process:

- rebuild image: docker-compose build --no-cache app
- rebuild container: docker-compose up --build --force-recreate --no-deps -d app
- When rebuilding an image: do not cache(–no-cache) layers. Then start it up, but first build(–build) container and then recreate it(–force-recreate) if it exists. As well do not restart dependencies(–no-deps).

This will remove all images ([reference](https://docs.docker.com/compose/reference/down/))
``
docker-compose down --rmi local/all
``
### References

[PAHO Python](https://github.com/eclipse/paho.mqtt.python)

[MQTT-Client-Connections](http://www.steves-internet-guide.com/client-connections-python-mqtt/)

[MQTT-Callback](http://www.steves-internet-guide.com/mqtt-python-callbacks/)

[Realtime-Data-Stream-Chart-in-Flask](https://github.com/grebtsew/Visualize-Realtime-Data-Stream-Chart-in-Flask)