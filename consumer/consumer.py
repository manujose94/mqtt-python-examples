
import sys
import os
dir_common = os.path.split(os.path.realpath(__file__))[0] + '/../'
sys.path.append(dir_common)  #  Add the root directory to the system directory , In order to quote properly common folder 
import paho.mqtt.client as mqtt
import time
MQTT_HOST = os.environ.get('MQTT_HOST', 'localhost')
MQTT_PORT = int(os.environ.get('MQTT_PORT', 1883))
MQTT_USER = os.environ.get('MQTT_USER', 'guest')
MQTT_TOPIC = "tracker"



def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    if rc==0:
        print("connected OK Returned code=",rc)
        client.subscribe(MQTT_TOPIC)
    else:
        print("Bad connection Returned code=",rc)

def on_message(client, userdata, msg):
    print(msg.topic+" "+msg.payload.decode("utf-8"))

def on_subscribe(mosq, obj, mid, granted_qos):
    print("Subscribed to Topic: " + 
	MQTT_TOPIC + " with QoS: " + str(granted_qos))

def client_loop():
    client_id = time.strftime('%Y%m%d%H%M%S',time.localtime(time.time()))
    client = mqtt.Client(client_id)  # ClientId Cannot repeat, so use the current time 
    client.username_pw_set("mamarbao", "mamarbao") #  Must be set, otherwise it will return" Connected with result code 4 " 
    client.on_connect = on_connect #bind call back function
    client.on_message = on_message #connect to broker
    client.on_subscribe = on_subscribe
    try:
        print(MQTT_HOST)
        client.connect(MQTT_HOST, 1883, 60)
    except ConnectionRefusedError as e:
        print("[ERROR] Conection Error to Broker MQTT")
        exit(1)
    client.loop_forever()


if __name__ == '__main__':
    client_loop()

#TODO:   TAKE a LOOK
# http://www.steves-internet-guide.com/client-connections-python-mqtt/
# https://codeburst.io/get-started-with-rabbitmq-on-docker-4428d7f6e46b